package com.stictpro.admin.entity;

import com.stictpro.service.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO
{
    private Long id;
    private String bookName;

    private String isbn;

    private String dateOfPublished;

    private Integer pageNumb;

    private String description;

    private String language;
    private Integer status;

    private String imageData;

    private MultipartFile image_posted1;

    private Category categoryItems;
    private MultipartFile pdfVersion;

    private String[] bookAuthorList;

    public String getFullImage1Url() {
        if (id != null && imageData != null) {
            return "/upload/book/" + id + "/image_posted1.png";
        } else {
            return "/upload/no_preview.jpg";
        }
    }
}
