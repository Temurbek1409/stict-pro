package com.stictpro.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDTO
{
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String description;

    //images
    private MultipartFile image_posted1;

    private String image1;

    public String getFullImage1Url() {
        if (id != null && image1 != null) {
            return "/upload/author/" + id + "/" + image1;
        } else {
            return "/upload/no_preview.jpg";
        }
    }
}
