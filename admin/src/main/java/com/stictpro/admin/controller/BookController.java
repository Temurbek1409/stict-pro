package com.stictpro.admin.controller;

import com.stictpro.admin.entity.BookDTO;
import com.stictpro.service.entity.Authors;
import com.stictpro.service.entity.Books;
import com.stictpro.service.service.AuthorService;
import com.stictpro.service.service.CategoryService;
import com.stictpro.service.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequiredArgsConstructor
@RequestMapping("/book")
public class BookController {
    private final CategoryService categoryService;
    private final AuthorService authorService;
    private final BookService bookService;
    private static final String redirect = "redirect:/book/book-list";
    private static final String redirectAfterDeleted = "redirect:/book/book-list?deleted";
    private static final String redirectAfterAdded = "redirect:/book/book-list?success";
    private static final String add_edit = "pages/books/addBooks";

    @GetMapping(value = "/addNewBook")
    public String addNewProduct(@ModelAttribute("productWrapper") BookDTO productWrapper,
                                Model model)
    {
        model.addAttribute("categories", categoryService.categoryList(false));
        model.addAttribute("authorList", authorService.authorDtoList(false));
        return add_edit;
    }

    @GetMapping(value = "/book-list")
    public String getBookList(@ModelAttribute("productWrapper") BookDTO productWrapper,
                                 Model model, BindingResult result) {
        model.addAttribute("bookList", bookService.bookDtoList(false));
        model.addAttribute("categories", categoryService.categoryList(false));
        model.addAttribute("authorList", authorService.authorDtoList(false));
        return "pages/books/bookList";
    }
    @GetMapping("/edit/{id}")
    public String updateBook(@PathVariable("id") Long id,Model model)
    {
        Books books=bookService.getOneBookDto(id);
        BookDTO bookDTO=new BookDTO();
        bookDTO.setId(books.getId());
        bookDTO.setBookName(books.getBookName());
        bookDTO.setDescription(books.getDescription());
        bookDTO.setImageData(books.getImageData());
        bookDTO.setImage_posted1(books.getImage_posted1());
        bookDTO.setDateOfPublished(books.getDateOfPublished());
        bookDTO.setLanguage(books.getLanguage());
        bookDTO.setIsbn(books.getIsbn());
        bookDTO.setStatus(books.isStatus()?1:0);
        bookDTO.setPageNumb(books.getPageNumb());
        bookDTO.setPdfVersion(books.getPdfVersion());
        bookDTO.setCategoryItems(books.getCategoryItems());
        String[] strings=new String[books.getBookAuthorList().size()];
        int i = 0;
        for (Authors author : books.getBookAuthorList()) {
            strings[i] = String.valueOf(author.getId());
            i++;
        }
        bookDTO.setBookAuthorList(strings);
        model.addAttribute("productWrapper",bookDTO);
        model.addAttribute("categories", categoryService.categoryList(false));
        model.addAttribute("authorList", authorService.authorDtoList(false));
       return add_edit;
    }

    @PostMapping(value = "/saveBook")
    public String saveNewProduct(@ModelAttribute("productWrapper") BookDTO productWrapper,
                                 Model model, BindingResult result) throws IOException, ParseException {
        model.addAttribute("productWrapper",productWrapper);
        model.addAttribute("categories", categoryService.categoryList(false));
        model.addAttribute("authorList", authorService.authorDtoList(false));
        if (result.hasErrors())
        {
            return "pages/books/addBooks";
        }

        Books booksDTO = new Books();
        Set<Authors> authorDTOS = new HashSet<>();
        for (String s : productWrapper.getBookAuthorList())
        {
            long productId = Long.parseLong(s);
            authorDTOS.add(authorService.getOneAuthorDto(productId));
        }

        booksDTO.setPageNumb(productWrapper.getPageNumb());
        booksDTO.setIsbn(productWrapper.getIsbn());
        booksDTO.setLanguage(productWrapper.getLanguage());
        booksDTO.setDateOfPublished(productWrapper.getDateOfPublished());
        booksDTO.setBookName(productWrapper.getBookName());
        booksDTO.setDescription(productWrapper.getDescription());
        booksDTO.setStatus(productWrapper.getStatus()==1?true:false);
        booksDTO.setImage_posted1(productWrapper.getImage_posted1());
        booksDTO.setImageData(productWrapper.getImageData());
        booksDTO.setPdfVersion(productWrapper.getPdfVersion());
        booksDTO.setCategoryItems(categoryService.categoryById(productWrapper.getCategoryItems().getId()));
        booksDTO.setBookAuthorList(authorDTOS);
        //update book by id
        if (productWrapper.getId() != null) {
            booksDTO.setId(productWrapper.getId());
            bookService.updateBook(booksDTO);
        } else {

            bookService.saveNewBook(booksDTO);
        }
        return redirectAfterAdded;
    }
    @RequestMapping(value = "/delete/{id}")
    public String deleteItemsByID(@PathVariable("id") Long id, Model model) {
        model.addAttribute("deleteMsg", bookService.deleteBook(id));
        return redirectAfterDeleted;
    }
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String addNewItems(@ModelAttribute("productWrapper") BookDTO bookDTO)
    {
        return null;
    }

}
