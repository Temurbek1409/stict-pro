package com.stictpro.admin.controller;

import com.stictpro.service.entity.Category;
import com.stictpro.service.service.AuthorService;
import com.stictpro.service.service.BookService;
import com.stictpro.service.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/category")
public class CategoryController {
    private final CategoryService categoryService;
    private static final String redirect = "redirect:/author/category-list";
    private static final String redirectAfterDeleted = "redirect:/category/category-list?deleted";
    private static final String redirectAfterAdded = "redirect:/category/category-list?success";
    private static final String add_edit = "pages/category/categoryList";

    @GetMapping(value = "/category-list")
    public String showAllCategory(Model model) {
        model.addAttribute("allCategory", categoryService.categoryList(false));
        return "pages/category/categoryList";
    }

    @GetMapping(value = "/deleteCategory/{id}")
    public String deleteAuthorsByID(@PathVariable("id") Long id, Model model) {
        model.addAttribute("deletedMsg", categoryService.deleteCategoryDTO(id));
        return redirectAfterDeleted;
    }

    @GetMapping("/add")
    public String getSavePage(@ModelAttribute("categoryDTO") Category categoryDTO, Model model) {
        model.addAttribute("categoryDTO", categoryDTO);
        return "/pages/category/addCategory";
    }

    @PostMapping(value = "/saveNewCategory")
    public String saveNewCategory(@ModelAttribute("categoryDTO") Category categoryDTO,
                                  BindingResult result, Model model) {
        model.addAttribute("categoryDTO", categoryDTO);
        model.addAttribute("allCategory", categoryService.categoryList(false));
        if (result.hasErrors()) {
            return "pages/category/addCategory";
        }
        if (categoryDTO.id == null) {
            categoryService.insertByCategoryDTO(categoryDTO);
        } else {
            categoryService.updateCategoryDTOById(categoryDTO.id, categoryDTO);
        }
        return redirectAfterAdded;
    }

    @GetMapping(value = "/edit/{id}")
    public String updateWithNewCategory(@PathVariable("id") Long id, Model model) {
        Category category = categoryService.categoryById(id);
        model.addAttribute("categoryDTO",category);
        return "/pages/category/addCategory";
    }

}
