package com.stictpro.admin.controller;

import com.stictpro.admin.entity.AuthorDTO;
import com.stictpro.service.entity.Authors;
import com.stictpro.service.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
@RequestMapping("/author")
public class AuthorController {
    private final AuthorService authorService;

    private static final String redirect = "redirect:/author/author-list";
    private static final String redirectAfterDeleted = "redirect:/author/author-list?deleted";
    private static final String redirectAfterAdded = "redirect:/author/author-list?success";
    private static final String add_edit = "pages/authors/addAuthors";

    @GetMapping(value = "/author-list")
    public String getAuthorList(Model model) {
        model.addAttribute("authorList", authorService.authorDtoList(false));
        return "pages/authors/authorList";
    }

    @GetMapping(value = "/delete/{id}")
    public String deleteAuthorsByID(@PathVariable("id") Long id, Model model) {
        model.addAttribute("deleteMessage", authorService.deleteAuthorById(id));
        return redirectAfterDeleted;
    }

    @GetMapping(value = "/add-author")
    public String getSavePage(Model model, @ModelAttribute("authorDtoData") AuthorDTO authorDtoData) {
        return add_edit;
    }

    @GetMapping(value = "/edit/{id}")
    public String editPage(@PathVariable("id") Long id,Model model)
    {
        model.addAttribute("authorDtoData",authorService.getOneAuthorDto(id));
        return add_edit;
    }
    @PostMapping(value = "/saveNewAuthors")
    public String insertNewPage(@ModelAttribute("authorDtoData") AuthorDTO authorWrapper,
                                Model model, BindingResult result) throws IOException {
        model.addAttribute("authorDtoData", authorWrapper);
        if (result.hasErrors()) {
            return add_edit;
        }
        Authors authorDTO = new Authors();
        if (authorWrapper.getId() != null) {
            authorDTO.setId(authorWrapper.getId());
        }
        authorDTO.setFirstName(authorWrapper.getFirstName());
        authorDTO.setPhoneNumber(authorWrapper.getPhoneNumber());
        authorDTO.setEmail(authorWrapper.getEmail());
        authorDTO.setLastName(authorWrapper.getLastName());
        authorDTO.setDescription(authorWrapper.getDescription());
        authorDTO.setImage1(authorWrapper.getImage1());
        authorDTO.setImage_posted1(authorWrapper.getImage_posted1());
        if (authorDTO.id != null) {
            authorService.updateAuthorById(authorWrapper.getId(), authorDTO);
        } else {
            authorService.saveAuthor(authorDTO);
        }
        return redirectAfterAdded;
    }
}
