package com.stictpro.admin.controller;


import com.stictpro.service.service.AuthorService;
import com.stictpro.service.service.BookService;
import com.stictpro.service.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class HomeController {
    private final BookService bookService;
    private final AuthorService authorService;
    private final CategoryService categoryService;

    @GetMapping
    public String getDashboard(Model model) {
        model.addAttribute("counterForBook",bookService.countTotalNumber());
        model.addAttribute("counterForAuthor",authorService.counterForAuthor());
        model.addAttribute("countForCategory",categoryService.counterForCategory());
        return "dashboard/index";
    }
}
