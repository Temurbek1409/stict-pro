package com.stictpro.admin.controller;

import com.stictpro.service.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/contact")
public class ContactController {
    private final ContactService contactService;

    @GetMapping("/list")
    public String getAllContacts(Model model) {
        model.addAttribute("ListContacts", contactService.contactsList());
        return "pages/contacts";
    }
}
