package com.stictpro.service.service.impl;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/***
 *This is for uploading class stuff which consists of file and image
 * @apiNote ImageUpload
 */

public final class ImageUpload {
    public ImageUpload() {
    }

    public static String convert(Long imageId, MultipartFile multipartFile, String authorOrBook) {
        String fileName = "";
        String productFolder = "";
        if ("author".equals(authorOrBook)) {
            productFolder = "admin/src/main/resources/static/upload/author";
            fileName = multipartFile.getName() + ".png";
        }
        if ("pdf".equals(authorOrBook)) {
            productFolder = "admin/src/main/resources/static/upload/book";
            fileName = multipartFile.getName() + ".pdf";
        }
        if ("book".equals(authorOrBook)){
            productFolder = "admin/src/main/resources/static/upload/book";
            fileName = multipartFile.getName() + ".png";
        }

        //Save image
        try {
            byte[] bytes = multipartFile.getBytes();
            //Create directory if not exists
            File file = new File(productFolder + "/" + imageId);
            if (!file.exists()) {
                file.mkdirs();
            }
            String fileWithFolderName = productFolder + "/" + imageId + "/" + fileName;
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(
                            new File(fileWithFolderName)));
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileName;
    }

    // delete directory method
    public static void deleteDirectory(File directory) {
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    // Recursively delete subdirectories
                    deleteDirectory(file);
                } else {
                    // Delete files in the directory
                    file.delete();
                }
            }
        }
        // Finally, delete the directory itself
        directory.delete();
    }
}
