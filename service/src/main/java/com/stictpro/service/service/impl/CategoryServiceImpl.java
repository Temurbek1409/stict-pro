package com.stictpro.service.service.impl;

import com.stictpro.service.entity.Category;
import com.stictpro.service.repository.CategoryRepository;
import com.stictpro.service.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;


    @Override
    public List<Category> categoryList(boolean data) {
        List<Category> categoryDTOList = categoryRepository.getCategoriesByDeletedOrDeletedNot(data);
        return categoryDTOList;
    }

    @Override
    public List<Category> getAllCategoryWithSubCategory() {
        return categoryRepository.findAll();
    }

    @Override
    public Category categoryById(long id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public void insertByCategoryDTO(Category categoryDTO) {
        categoryRepository.save(categoryDTO);
    }

    @Override
    public boolean deleteCategoryDTO(long id) {
        Category category = categoryById(id);
        categoryRepository.delete(category);
        return true;
    }

    @Override
    public void updateCategoryDTOById(long id, Category categoryDTO) {
        categoryRepository.save(categoryDTO);
    }

    @Override
    public int counterForCategory() {
        return categoryRepository.countCategoryBy();
    }
}
