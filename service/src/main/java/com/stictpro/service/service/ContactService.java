package com.stictpro.service.service;

import com.stictpro.service.entity.Contacts;
import com.stictpro.service.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;

    public List<Contacts> contactsList()
    {
        return contactRepository.findAll();
    }

    public void deleteContactById(Long id)
    {
        contactRepository.deleteById(id);
    }
    public void saveContact(Contacts contacts)
    {
            contactRepository.save(contacts);
    }
}
