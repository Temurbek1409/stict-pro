package com.stictpro.service.service.impl;


import com.stictpro.service.entity.Authors;
import com.stictpro.service.repository.AuthorRepository;
import com.stictpro.service.repository.BookRepository;
import com.stictpro.service.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    @Override
    public List<Authors> authorDtoList(boolean info) {
        List<Authors> allByAuthorByDeletedOrNotDeleted = authorRepository.getAllByAuthorByDeletedOrNotDeleted(info);
        return allByAuthorByDeletedOrNotDeleted;
    }

    @Override
    public Authors getOneAuthorDto(long id) {
        return authorRepository.findById(id).get();
    }

    @Override
    public void saveAuthor(Authors authorDTO) throws IOException {
        authorRepository.save(authorDTO);
        if (authorDTO.getImage_posted1().getSize() > 0) {
            String image1 = ImageUpload.convert(authorDTO.getId(), authorDTO.getImage_posted1(), "author");
            authorDTO.setImage1(image1);
            authorRepository.save(authorDTO);
        }

    }

    @Override
    public boolean deleteAuthorById(long id) {
        Authors authors = getOneAuthorDto(id);
        authors.removeAllProducts();
        String previousImageFilePath = "admin/src/main/resources/static/upload/author/" + authors.getId();
        File file = new File(previousImageFilePath);
        if (file.exists() && file.isDirectory()) {
            // Delete the previous image file
            ImageUpload.deleteDirectory(file);
        }
        authorRepository.delete(authors);
        return true;
    }

    @Override
    public void updateAuthorById(long id, Authors authorDTO) {
        Authors author = authorRepository.findById(id).get();
        author.removeAllProducts();
        authorRepository.save(author);
        if (authorDTO.getImage_posted1().getSize() != 0) {
            author.setImage_posted1(authorDTO.getImage_posted1());
            String image1 = ImageUpload.convert(authorDTO.getId(), authorDTO.getImage_posted1(), "author");
            authorDTO.setImage1(image1);
            authorRepository.save(authorDTO);
        }
//        updateProduct(authorDTO);
        authorRepository.save(authorDTO);
    }

    @Override
    public Page<Authors> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return this.authorRepository.findAll(pageable);
    }

    @Override
    public int counterForAuthor() {
        return authorRepository.countByAuthors();
    }
}
