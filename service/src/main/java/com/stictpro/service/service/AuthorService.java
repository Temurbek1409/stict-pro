package com.stictpro.service.service;



import com.stictpro.service.entity.Authors;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

/**
 * @apiNote  Extra and CRUD services for entity Author
 * @author Temurbek
 * @version 1.0
 */
public interface AuthorService
{
    List<Authors> authorDtoList(boolean info);
    Authors getOneAuthorDto(long id);
    void saveAuthor(Authors authorDTO) throws IOException;
    boolean deleteAuthorById(long id);
    void updateAuthorById(long id,Authors authorDTO);
    Page<Authors> findPaginated(int pageNo, int pageSize);
    int counterForAuthor();
}
