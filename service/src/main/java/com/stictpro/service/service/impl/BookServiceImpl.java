package com.stictpro.service.service.impl;

import com.stictpro.service.entity.Books;
import com.stictpro.service.repository.AuthorRepository;
import com.stictpro.service.repository.BookRepository;
import com.stictpro.service.repository.CategoryRepository;
import com.stictpro.service.service.BookService;
import lombok.RequiredArgsConstructor;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/***
 * @see BookService
 * @author Temurbek
 * @version 1.0
 */
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final CategoryRepository categoryRepository;
    private final AuthorRepository authorRepository;

    @Override
    public int countTotalNumber() {
        return bookRepository.getAllBy();
    }

    @Override
    public List<Books> bookDtoList(boolean info) {
        List<Books> booksList = bookRepository.getAllByBooksIfNotDeleted(info);
        return booksList;
    }
    //Get Books by Name
    @Override
    public List<Books> getBookByName(String name) {
        List<Books> listList=bookRepository.getAllByBookName(name);
        return listList;
    }

    @Override
    public List<Books> getBookByAuthorsName(String s) {
        List<Books> booksList=bookRepository.getAllByBookAuthorList(s);
        return booksList;
    }
    //get Books by category id


    @Override
    public int counterByCategoryId(long id) {
        return bookRepository.counterBookByCategory(id);
    }

    @Override
    public List<Books> getBookByCategory(long id) {
        List<Books> booksList=bookRepository.getBooksByCategoryItems(id);
        return booksList.isEmpty()?null:booksList;
    }

    @Override
    public Books getOneBookDto(long id) {
        return bookRepository.findById(id).get();
    }

    @Override
    public void saveNewBook(Books booksDTO) throws IOException, ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date yourDate = dateFormat.parse(booksDTO.getDateOfPublished()); // Replace with the actual date you want to save
        String dateAsString = dateFormat.format(yourDate);
        booksDTO.setDateOfPublished(dateAsString);

        // Save the book
        bookRepository.save(booksDTO);

        // Handle image if provided
        handleBookImage(booksDTO);
        bookRepository.save(booksDTO);
    }

    private void handleBookImage(Books booksDTO) {
        if (booksDTO.getImage_posted1() != null && booksDTO.getImage_posted1().getSize() > 0) {
            String image1=ImageUpload.convert(booksDTO.getId(), booksDTO.getImage_posted1(),"book");
            String bookpdf=ImageUpload.convert(booksDTO.getId(),booksDTO.getPdfVersion(),"pdf");
            booksDTO.setImageData(image1);
        }
    }

    @Override
    public void updateBook(Books booksDTO) throws ParseException {

        Books books1 = bookRepository.findById(booksDTO.getId()).orElse(null);

        books1.setBookName(booksDTO.getBookName());
        books1.setStatus(booksDTO.isStatus());
        books1.setDescription(booksDTO.getDescription());
        books1.setLanguage(booksDTO.getLanguage());
        //date
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date yourDate = dateFormat.parse(booksDTO.getDateOfPublished()); // Replace with the actual date you want to save
        String dateAsString = dateFormat.format(yourDate);
        books1.setDateOfPublished(dateAsString);
        //end
//        books1.setDateOfPublished(booksDTO.getDateOfPublished());
        books1.setIsbn(booksDTO.getIsbn());
        books1.setImage_posted1(booksDTO.getImage_posted1());
        books1.setImageData(booksDTO.getImageData());
        books1.setPageNumb(booksDTO.getPageNumb());
        books1.setPdfVersion(booksDTO.getPdfVersion());
        books1.setCategoryItems(books1.getCategoryItems());
        books1.removeAuthors();
        books1.setBookAuthorList(booksDTO.getBookAuthorList());
        try {
            saveNewBook(books1);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
//        bookRepository.save(books1);
    }

    @Override
    public boolean deleteBook(long id) {
        Books books=getOneBookDto(id);
        books.removeAuthors();
                if (books != null) {
            if (books.getImageData() != null) {
                String previousImageFilePath = "admin/src/main/resources/static/upload/book/" + books.getId();
                File previousImageFile = new File(previousImageFilePath);
                if (previousImageFile.exists()&&previousImageFile.isDirectory()) {
                    // Delete the previous image file
                    ImageUpload.deleteDirectory(previousImageFile);
                }
            }
        }

        bookRepository.delete(books);
        return true;
    }

}
