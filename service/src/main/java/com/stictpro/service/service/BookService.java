package com.stictpro.service.service;

import com.stictpro.service.entity.Books;

import java.awt.print.Book;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * @apiNote  Extra and CRUD services for entity Book
 * @author Temurbek
 * @version 1.0
 */
public interface BookService
{
    List<Books> bookDtoList(boolean info);
    Books getOneBookDto(long id);
    void saveNewBook(Books booksDTO) throws IOException, ParseException;
    void updateBook(Books booksDTO) throws ParseException;
    boolean deleteBook(long id);
    int countTotalNumber();
    List<Books> getBookByName(String name);
    List<Books> getBookByAuthorsName(String s);
    List<Books> getBookByCategory(long id);
    //book counter by categoryId
    int counterByCategoryId(long id);
}
