package com.stictpro.service.repository;

import com.stictpro.service.entity.Authors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Authors,Long>
{
    //get Author by id
    Authors getAuthorById(long id);
    // get Author by id if it is not deleted
    @Query(value = "select * from authors s where s.is_deleted=:deleted limit 4", nativeQuery = true)
    List<Authors> getAllByAuthorByDeletedOrNotDeleted(boolean deleted);
    @Query(value = "select count(*) from authors s", nativeQuery = true)
    int countByAuthors();

}
