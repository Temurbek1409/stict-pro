package com.stictpro.service.repository;

import com.stictpro.service.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>
{
    @Query(value = "select * from category s where s.is_deleted=:deleted", nativeQuery = true)
    List<Category> getCategoriesByDeletedOrDeletedNot(boolean deleted);

    Category findCategoryById(long id);
    @Query(value = "select count(*) from category s", nativeQuery = true)
    int countCategoryBy();
}

