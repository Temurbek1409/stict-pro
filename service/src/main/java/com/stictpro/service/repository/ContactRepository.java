package com.stictpro.service.repository;

import com.stictpro.service.entity.Contacts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contacts,Long>
{

}
