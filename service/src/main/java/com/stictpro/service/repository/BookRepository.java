package com.stictpro.service.repository;
import com.stictpro.service.entity.Books;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Books,Long>
{
    //get all Products by id if it is not deleted
    @Query(value = "select * from book s where s.is_deleted=:deleted order by s.created_at limit 6", nativeQuery = true)
    List<Books> getAllByBooksIfNotDeleted(boolean deleted);
    // get product by id
    Books findProductById(long id);
    @Query(value = "select count(*) from book s", nativeQuery = true)
    int getAllBy();
    // get books by bookName
    @Query(value = "select * from book s where s.title=:titleName",nativeQuery = true)
    List<Books> getAllByBookName(String titleName);
    //get books by category id
    @Query(value = "select * from book s where s.title=:titleName",nativeQuery = true)
    List<Books> getBooksByBookName();
    // get books by authors name
    @Query(value = "SELECT * FROM public.authors b " +
            "INNER JOIN public.product_author pa " +
            "ON b.id = pa.author_id " +
            "INNER JOIN public.book books " +
            "ON pa.product_id = books.id " +
            "WHERE b.first_name = :nameAuthor OR b.last_name = :nameAuthor", nativeQuery = true)
    List<Books> getAllByBookAuthorList(String nameAuthor);

    //get books by category
    @Query(value = "select * from book s where s.category_id=:id",nativeQuery = true)
    List<Books> getBooksByCategoryItems(long id);

    //count category by book id
    @Query(value = "SELECT count(*) FROM book s where s.category_id=:id",nativeQuery = true)
    int counterBookByCategory(long id);
}
