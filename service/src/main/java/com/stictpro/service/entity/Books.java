package com.stictpro.service.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stictpro.service.entity.template.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "book")
public class Books extends BaseEntity {
    @Column(name = "title")
    @NotNull
    private String bookName;

    @Column(name = "ISBN")
    @NotNull
    private String isbn;

    @Column(name = "publication_Date")
    @NotNull
    private String dateOfPublished;

    @Column(name = "number_of_Pages")
    @NotNull
    private Integer pageNumb;

    @Column(name = "description")
    @NotNull
    private String description;

    @Column(name = "Language")
    @NotNull
    private String language;

    @Column(name = "status")
    private boolean status = false;

    //image param
    @Column(name = "image")
    private String imageData;

    @Transient
    private MultipartFile image_posted1;
    @Transient
    private MultipartFile pdfVersion;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "product_author",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    @JsonIgnore
    private Set<Authors> bookAuthorList = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category categoryItems;

    public String getFullImage1Url() {
        if (id != null && imageData != null) {
            return "/upload/book/" + id + "/" + imageData;
        } else {
            return "/upload/no_preview.jpg";
        }
    }

    public String getBookPdfUrl() {
        if (id != null && imageData != null) {
            return "/upload/book/" + id + "/book.pdf";
        } else {
            return "/upload/no_preview.jpg";
        }
    }

    public void addAuthor(Authors author) {
        this.bookAuthorList.add(author);
        author.getBooksDTOSet().add(this);
    }

    public void removeAuthor(Authors author) {
        this.getBookAuthorList().remove(author);
        author.getBooksDTOSet().remove(this);
    }

    public void removeAuthors() {
        for (Authors author : new HashSet<>(bookAuthorList)) {
            removeAuthor(author);
        }
    }
}
