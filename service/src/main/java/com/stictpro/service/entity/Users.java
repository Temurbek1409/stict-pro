package com.stictpro.service.entity;

import com.stictpro.service.entity.template.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class Users extends BaseEntity {
    @NotEmpty(message = "First name can't be empty!")
    @Column(name = "firstName") // Corrected column name
    private String firstName;

    @NotEmpty(message = "Last name can't be empty!")
    @Column(name = "lastName") // Corrected column name
    private String lastName;

    @Column(name = "username")
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    private String username;

    @Column(name = "password")
    @Length(min = 5, message = "*Your password must have at least 5 characters")
    private String password;

    @Column(name = "is_active")
    private Boolean isActive;
}
