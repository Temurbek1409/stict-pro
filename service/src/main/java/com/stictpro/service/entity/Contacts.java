package com.stictpro.service.entity;

import com.stictpro.service.entity.template.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contacts")
public class Contacts extends BaseEntity {
    @Column(name = "name", unique = true)
    @NotNull
    private String firstName;

    @Column(name = "email")
    @NotNull
    private String email;

    @Column(name = "subject")
    @NotNull
    private String subjects;

    @Column(name = "message")
    @NotNull
    private String msg;

}
