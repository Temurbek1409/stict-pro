package com.stictpro.service.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stictpro.service.entity.template.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "authors")
public class Authors extends BaseEntity
{
    @Column(name = "firstName",unique = true)
    @NotNull
    private String firstName;

    @Column(name = "lastName")
    @NotNull
    private String lastName;

    @Column(name = "email")
    @NotNull
    private String email;

    @Column(name = "phoneNumber")
    @NotNull
    private String phoneNumber;

    @Column(name = "description")
    @NotNull
    private String description;

    //images
    @Transient
    private MultipartFile image_posted1;

    @Column(name = "image1")
    private String image1;

    @ManyToMany(mappedBy = "bookAuthorList")
    @JsonIgnore
    private Set<Books> booksDTOSet = new HashSet<>();

    public void removeBook(Books books) {
        this.getBooksDTOSet().remove(books);
        books.getBookAuthorList().remove(this);
    }
    public void addBook(Books books) {
        this.booksDTOSet.add(books);
        books.getBookAuthorList().add(this);
    }


    public void removeAllProducts() {
        for (Books books : new HashSet<>(booksDTOSet)) {
            removeBook(books);
        }
    }

    public String getFullImage1Url() {
        if (id != null && image1 != null) {
            return "/upload/author/" + id + "/" + image1;
        } else {
            return "/upload/no_preview.jpg";
        }
    }
}
