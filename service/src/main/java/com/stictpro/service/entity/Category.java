package com.stictpro.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stictpro.service.entity.template.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "category")
public class Category extends BaseEntity
{
    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "description")
    @NotNull
    private String description;

    @OneToMany(mappedBy = "categoryItems", cascade = CascadeType.PERSIST)
    @JsonIgnore
    private Set<Books> categoryBooks = new HashSet<>();
}
