package com.stictpro.service.entity;

import com.stictpro.service.entity.template.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "role")
public class Role extends BaseEntity
{
    @Column(name = "roleName")
    private String roleName;

}
