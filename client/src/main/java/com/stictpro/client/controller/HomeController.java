package com.stictpro.client.controller;

import com.stictpro.client.service.CategoryToCounter;
import com.stictpro.service.entity.Contacts;
import com.stictpro.service.service.AuthorService;
import com.stictpro.service.service.BookService;
import com.stictpro.service.service.CategoryService;
import com.stictpro.service.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequiredArgsConstructor
public class HomeController {
    private final BookService bookService;
    private final CategoryService categoryService;
    private final AuthorService authorService;
    private final ContactService contactService;
    private final CategoryToCounter categoryToCounter;

    @RequestMapping("/")
    public String getHomePage(Model model) {
        model.addAttribute("categoryList", categoryService.categoryList(false));
        return "home/homePage";
    }
    // Contact info
    @RequestMapping("/contact")
    public String getContactPage(@ModelAttribute("contacts") Contacts contacts,
                                 Model model, BindingResult result)
    {
        return "pages/contactPage";
    }
    @RequestMapping("/reading")
    public String getReadingBook(@RequestParam("id") Long id, Model model)
    {
        return "";
    }
    @RequestMapping("/search")
    public String findBookAndAuthor(@RequestParam("searchText") String searchText, Model model)
    {
        model.addAttribute("bookListByName",bookService.getBookByName(searchText));
        model.addAttribute("bookListByAuthorName",bookService.getBookByAuthorsName(searchText));
        model.addAttribute("searchItem",searchText);
        model.addAttribute("categoryList",categoryToCounter.categoryBookCounterList(categoryService.categoryList(false)));
        return "pages/searchItems";
    }
    @RequestMapping("/searchByCategory")
    public String findBookByCategory(@RequestParam("id") Long id, Model model)
    {
        model.addAttribute("categoryList",categoryToCounter.categoryBookCounterList(categoryService.categoryList(false)));
        model.addAttribute("bookListByCategory",bookService.getBookByCategory(id));
        return "pages/searchBookCategory";
    }

    @RequestMapping(value = "/contact/save", method = RequestMethod.POST)
    public String savePost(@ModelAttribute("contacts") Contacts contacts,
                           Model model, BindingResult result) {
        model.addAttribute("contacts", contacts);
        contactService.saveContact(contacts);
        return "redirect:/contact?success";
    }

    @RequestMapping("/get")
    public String getItems(Model model) {
        model.addAttribute("categoryList",categoryService.categoryList(false));
        model.addAttribute("bookList",bookService.bookDtoList(false));
        return "pages/searchItems";
    }
}
