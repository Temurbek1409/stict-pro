package com.stictpro.client.service;

import com.stictpro.client.entity.CategoryBookCounter;
import com.stictpro.service.entity.Category;
import com.stictpro.service.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryToCounter {
    private final BookRepository bookRepository;

    public List<CategoryBookCounter> categoryBookCounterList(List<Category> categoryList) {
        List<CategoryBookCounter> categoryBookCounterList = new ArrayList<>();
        CategoryBookCounter categoryBookCounter;
        for (Category category : categoryList) {
            categoryBookCounter = new CategoryBookCounter();
            categoryBookCounter.setId(category.getId());
            categoryBookCounter.setName(category.getName());
            categoryBookCounter.setBookCount(bookRepository.counterBookByCategory(category.id));
            categoryBookCounterList.add(categoryBookCounter);
        }
        return categoryBookCounterList;
    }
}
